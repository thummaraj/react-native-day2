import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, TextInput, TouchableOpacity, Image} from 'react-native';

type Props = {};
export default class App2 extends Component<Props>{
    constructor(props) {
    super(props)
    this.state = { username: ''},
    this.state = { password: ''}
    }

    render(){
        return(
            <View style={styles.containers}> 
            
            <View style={[styles.layer1, styles.center]}>
            <View style={[styles.circle, styles.center]}>
            <Image source={require('./cat.jpg')} style={[styles.circle, styles.center]} />
            </View>
            </View>
            
            <View >
                <Text style={{ alignItems: 'center'}}>username: {this.state.username} - password: {this.state.password}</Text>
            <TextInput onChangeText={ (username) => {this.setState({username})}}  
            style={{ height: 40, borderColor: 'gray', borderWidth: 1, backgroundColor: 'white', margin: 14 }}></TextInput>
            <TextInput onChangeText={ (value) => {this.setState({password: value})}}  
            style={{ height: 40, borderColor: 'gray', borderWidth: 1, backgroundColor: 'white', margin: 14 }}></TextInput>
            </View>


            <TouchableOpacity>
            <View style={[styles.rectangle1,styles.center]}>
            <Text style={styles.rectangleText2}>TouchableOpacity</Text>
            </View>
            </TouchableOpacity>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    containers: {
        backgroundColor: 'pink', 
        flex: 1,
        justifyContent: 'center',
        
    }, 
    center: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    circle: {
        width: 200,
        height: 200,
        borderRadius: 200/2,
        backgroundColor: 'white',
    },
    circleText: {
        color: 'black',
       
    },
    layer1: {
        
        flex: 1,
        
        
    },
    layer2: {
        
        flex: 1,
        
    },
    rectangle: {
        backgroundColor: 'white',
        padding: 20,
        margin: 14
    
    },
    rectangle1: {
        backgroundColor: 'black',
        padding: 20,
        margin: 14
    
    },
    rectangleText: {
        color: 'black',
    },
    rectangleText2: {
        color: 'white',
        
        

    },
  
})