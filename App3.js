import React, { Component } from "react";
import {
  Button,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  Modal,
  Alert,
  ScrollView
} from "react-native";

type Props = {};
export default class App3 extends Component<Props> {
  state = {
    isShowModal: false,
    images: ""
  };

  showModal(visible) {
    this.setState({ isShowModal: visible });
  }
  showImage(image) {
    this.setState({ images: image });
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Text style={styles.headerText}>Cats</Text>
        </View>

        <ScrollView>
          <View style={styles.content}>
            <View style={styles.row}>
              <TouchableOpacity
                onPress={() => {
                  this.showModal(true);
                  this.showImage( require("./cat7.jpg"));
                }}
              >
                <View style={styles.box1}>
                  <Image
                    source={require("./cat7.jpg")}
                    style={[styles.row, styles.box]}
                  />
                  <Text>cat1</Text>
                </View>
              </TouchableOpacity>

              <Modal  
      transparent={true}
      visible={this.state.isShowModal}>
      
      <View style={[styles.modal]}>
     <Button color="#e60000" title="Hide Modal" onPress={() => { this.showModal(!this.state.isShowModal)}}></Button>
     <Image source={this.state.images} style={styles.imgModal} resizeMode="stretch"></Image>
     </View>

      </Modal>

              <TouchableOpacity onPress={() => {
                  this.showModal(true);
                  this.showImage(require( "./cat2.jpg"));
                }}>
                <View style={styles.box2}>
                  <Image
                    source={require("./cat2.jpg")}
                    style={[styles.row, styles.box]}
                  />
                  <Text>cat2</Text>
                </View>
              </TouchableOpacity>
            </View>

            <View style={styles.row}>
              <TouchableOpacity onPress={() => {
                  this.showModal(true);
                  this.showImage( require("./cat3.jpg"));
                }}>
                <View style={styles.box1}>
                  <Image
                    source={require("./cat3.jpg")}
                    style={[styles.row, styles.box]}
                  />
                  <Text>cat3</Text>
                </View>
              </TouchableOpacity>

              <TouchableOpacity onPress={() => {
                  this.showModal(true);
                  this.showImage(require("./cat4.jpg"));
                }}>
                <View style={styles.box2}>
                  <Image
                    source={require("./cat4.jpg")}
                    style={[styles.row, styles.box]}
                  />
                  <Text>cat4</Text>
                </View>
              </TouchableOpacity>
            </View>

            <View style={styles.row}>
              <TouchableOpacity onPress={() => {
                  this.showModal(true);
                  this.showImage( require("./cat5.jpg"));
                }}>
                <View style={styles.box1}>
                  <Image
                    source={require("./cat5.jpg")}
                    style={[styles.row, styles.box]}
                  />
                  <Text>cat5</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => {
                  this.showModal(true);
                  this.showImage(require("./cat6.jpg") );
                }}>
                <View style={styles.box2}>
                  <Image
                    source={require("./cat6.jpg")}
                    style={[styles.row, styles.box]}
                  />
                  <Text>cat6</Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
      </View>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "pink"
  },
  header: {
    alignItems: "center"
  },
  headerText: {
    color: "white",
    fontSize: 20,
    fontWeight: "bold",
    padding: 40
  },
  content: {
    flex: 1,
    flexDirection: "column"
  },

  box1: {
    flex: 1,
    margin: 14,
    height: 200
  },
  box2: {
    flex: 1,
    margin: 14,
    height: 200
  },
  row: {
    backgroundColor: "pink",
    flex: 1,
    flexDirection: "row"
  },
  box: {
    width: 150,
    height: 120
  },
  modal: {
    marginLeft: 30,
    marginTop: 30,
    width: '80%',
    height: '80%',
    backgroundColor: 'white',
},
imgModal: {
  width: '100%',
  height: '100%',
    padding: 50
},

});
